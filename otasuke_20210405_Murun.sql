/*
SQLyog Ultimate - MySQL GUI v8.22 
MySQL - 5.5.5-10.4.17-MariaDB : Database - otasuke
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`otasuke` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `otasuke`;

/*Table structure for table `ot_building_type` */

DROP TABLE IF EXISTS `ot_building_type`;

CREATE TABLE `ot_building_type` (
  `building_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_desc` varchar(200) DEFAULT NULL,
  `enable_flag` tinyint(1) DEFAULT 1,
  `order_view` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`building_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_building_type` */

/*Table structure for table `ot_company` */

DROP TABLE IF EXISTS `ot_company`;

CREATE TABLE `ot_company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) DEFAULT NULL,
  `enable_flag` tinyint(1) DEFAULT NULL,
  `order_view` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `ot_company` */

insert  into `ot_company`(`company_id`,`company_name`,`enable_flag`,`order_view`) values (1,'会社A',1,1),(2,'会社B',1,2),(3,'会社C',1,3);

/*Table structure for table `ot_deal_comment` */

DROP TABLE IF EXISTS `ot_deal_comment`;

CREATE TABLE `ot_deal_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_desc` varchar(200) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `deal_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deal_amount` double(10,2) DEFAULT NULL,
  `user_type` varchar(50) DEFAULT NULL COMMENT 'CUSTOMER CREATOR',
  `proposal_start_date` date DEFAULT NULL,
  `proposal_end_date` date DEFAULT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `IXFK_ot_deal_comment_ot_user` (`user_id`),
  KEY `IXFK_ot_deal_comment_ot_work_deal` (`deal_id`),
  CONSTRAINT `FK_ot_deal_comment_ot_user` FOREIGN KEY (`user_id`) REFERENCES `ot_user` (`user_id`),
  CONSTRAINT `FK_ot_deal_comment_ot_work_deal` FOREIGN KEY (`deal_id`) REFERENCES `ot_work_deal` (`deal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_deal_comment` */

/*Table structure for table `ot_favourite` */

DROP TABLE IF EXISTS `ot_favourite`;

CREATE TABLE `ot_favourite` (
  `favourite_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `work_id` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`favourite_id`),
  KEY `IXFK_ot_favourite_ot_user` (`user_id`),
  KEY `IXFK_ot_favourite_ot_work` (`work_id`),
  CONSTRAINT `FK_ot_favourite_ot_user` FOREIGN KEY (`user_id`) REFERENCES `ot_user` (`user_id`),
  CONSTRAINT `FK_ot_favourite_ot_work` FOREIGN KEY (`work_id`) REFERENCES `ot_work` (`work_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ot_favourite` */

insert  into `ot_favourite`(`favourite_id`,`user_id`,`work_id`,`created_date`) values (1,1,10,'2020-05-05 00:00:00'),(2,1,8,'2021-04-02 00:00:00');

/*Table structure for table `ot_floor_plan` */

DROP TABLE IF EXISTS `ot_floor_plan`;

CREATE TABLE `ot_floor_plan` (
  `floor_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `floor_plan_desc` varchar(200) DEFAULT NULL,
  `enable_flag` tinyint(1) DEFAULT 1,
  `order_view` int(11) DEFAULT NULL,
  PRIMARY KEY (`floor_plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='1DK, 2DK';

/*Data for the table `ot_floor_plan` */

/*Table structure for table `ot_jp_calendar` */

DROP TABLE IF EXISTS `ot_jp_calendar`;

CREATE TABLE `ot_jp_calendar` (
  `calendar_id` int(11) NOT NULL AUTO_INCREMENT,
  `gregorian_year` year(4) DEFAULT NULL,
  `jp_name` varchar(50) DEFAULT NULL,
  `jp_year` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`calendar_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_jp_calendar` */

/*Table structure for table `ot_license` */

DROP TABLE IF EXISTS `ot_license`;

CREATE TABLE `ot_license` (
  `license_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_occupation_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`license_id`),
  KEY `IXFK_ot_license_ot_user` (`user_occupation_id`),
  KEY `IXFK_ot_license_ot_user_occupation` (`user_occupation_id`),
  CONSTRAINT `FK_ot_license_ot_user_occupation` FOREIGN KEY (`user_occupation_id`) REFERENCES `ot_user_occupation` (`user_occupation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='License images';

/*Data for the table `ot_license` */

/*Table structure for table `ot_notification` */

DROP TABLE IF EXISTS `ot_notification`;

CREATE TABLE `ot_notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `notification_desc` varchar(500) DEFAULT NULL,
  `read_flag` tinyint(1) DEFAULT NULL,
  `read_date` timestamp NULL DEFAULT NULL,
  `del_flag` tinyint(1) DEFAULT NULL,
  `del_date` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `notification_type` varchar(2) DEFAULT NULL COMMENT 'N - Notification F - Feedback',
  `created_from` varchar(50) DEFAULT NULL COMMENT 'C-Customer S-System',
  `work_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`notification_id`),
  KEY `IXFK_ot_notification_ot_user` (`user_id`),
  KEY `IXFK_ot_notification_ot_work` (`work_id`),
  CONSTRAINT `FK_ot_notification_ot_user` FOREIGN KEY (`user_id`) REFERENCES `ot_user` (`user_id`),
  CONSTRAINT `FK_ot_notification_ot_work` FOREIGN KEY (`work_id`) REFERENCES `ot_work` (`work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_notification` */

/*Table structure for table `ot_occupation` */

DROP TABLE IF EXISTS `ot_occupation`;

CREATE TABLE `ot_occupation` (
  `occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `occupation_desc` varchar(100) DEFAULT NULL,
  `enable_flag` tinyint(1) DEFAULT 1,
  `order_view` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`occupation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*Data for the table `ot_occupation` */

insert  into `ot_occupation`(`occupation_id`,`occupation_desc`,`enable_flag`,`order_view`) values (1,'大工工事',1,1),(2,'左官工事',1,2),(3,'とび・土工・コンクリート工事',1,3),(4,'石工事',1,4),(5,'屋根工事',1,5),(6,'電気工事',1,6),(7,'管工事',1,7),(8,'タイル・れんが・ブロック工事',1,8),(9,'鋼構造物工事',1,9),(10,'鉄筋工事',1,10),(11,'舗装工事',1,11),(12,'しゅんせつ工事',1,12),(13,'板金工事',1,13),(14,'ガラス工事',1,14),(15,'塗装工事',1,15),(16,'防水工事',1,16),(17,'内装仕上工事',1,17),(18,'機械器具設置工事',1,18),(19,'熱絶縁工事',1,19),(20,'電気通信工事',1,20),(21,'造園工事',1,21),(22,'さく井工事',1,22),(23,'建具工事',1,23),(24,'水道施設工事',1,24),(25,'消防施設工事',1,25),(26,'清掃施設工事',1,26),(27,'解体工事',1,27);

/*Table structure for table `ot_occupation_category` */

DROP TABLE IF EXISTS `ot_occupation_category`;

CREATE TABLE `ot_occupation_category` (
  `occupation_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `occupation_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`occupation_category_id`),
  KEY `IXFK_ot_occupation_category_ot_occupation` (`occupation_id`),
  KEY `IXFK_ot_skill_category_ot_skill` (`occupation_id`),
  KEY `IXFK_ot_skill_category_ot_work_category` (`category_id`),
  CONSTRAINT `FK_ot_occupation_category_ot_occupation` FOREIGN KEY (`occupation_id`) REFERENCES `ot_occupation` (`occupation_id`),
  CONSTRAINT `FK_ot_skill_category_ot_work_category` FOREIGN KEY (`category_id`) REFERENCES `ot_work_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_occupation_category` */

/*Table structure for table `ot_prefecture` */

DROP TABLE IF EXISTS `ot_prefecture`;

CREATE TABLE `ot_prefecture` (
  `prefecture_id` int(11) NOT NULL AUTO_INCREMENT,
  `prefecture_name` varchar(200) DEFAULT NULL,
  `enable_flag` tinyint(1) DEFAULT 1,
  `order_view` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`prefecture_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

/*Data for the table `ot_prefecture` */

insert  into `ot_prefecture`(`prefecture_id`,`prefecture_name`,`enable_flag`,`order_view`) values (1,'北海道',1,1),(2,'青森',1,2),(3,'岩手',1,3),(4,'宮城',1,4),(5,'秋田',1,5),(6,'山形',1,6),(7,'福島',1,7),(8,'茨城',1,8),(9,'栃木',1,9),(10,'群馬',1,10),(11,'埼玉',1,11),(12,'千葉',1,12),(13,' 東京',1,13),(14,'神奈川',1,14),(15,'新潟',1,15),(16,'富山',1,16),(17,'石川',1,17),(18,'福井',1,18),(19,'山梨',1,19),(21,'長野',1,20),(22,'岐阜',1,21),(23,'静岡',1,22),(24,'愛知',1,23),(25,'三重',1,24),(26,'滋賀',1,25),(27,'京都',1,26),(28,'大阪',1,27),(29,'兵庫',1,28),(30,'奈良',1,29),(31,'和歌山',1,30),(32,'鳥取',1,31),(33,'島根',1,32),(34,'岡山',1,33),(35,'広島',1,34),(36,'山口',1,35),(37,'徳島',1,36),(38,'香川',1,37),(39,'愛媛',1,38),(40,'高知',1,39),(41,'福岡',1,40),(42,'佐賀',1,41),(43,'長崎',1,42),(44,'熊本',1,43),(45,'大分',1,44),(46,'宮崎',1,45),(47,'鹿児島',1,46),(48,'沖縄',1,47);

/*Table structure for table `ot_public_holiday` */

DROP TABLE IF EXISTS `ot_public_holiday`;

CREATE TABLE `ot_public_holiday` (
  `holiday_id` int(11) NOT NULL AUTO_INCREMENT,
  `holyday_date` date DEFAULT NULL,
  `enable_flag` tinyint(1) DEFAULT NULL,
  `order_view` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`holiday_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_public_holiday` */

/*Table structure for table `ot_rate` */

DROP TABLE IF EXISTS `ot_rate`;

CREATE TABLE `ot_rate` (
  `rate_id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `feedback` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`rate_id`),
  KEY `IXFK_ot_rate_ot_user` (`user_id`),
  CONSTRAINT `FK_ot_rate_ot_user` FOREIGN KEY (`user_id`) REFERENCES `ot_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_rate` */

/*Table structure for table `ot_skill` */

DROP TABLE IF EXISTS `ot_skill`;

CREATE TABLE `ot_skill` (
  `skill_id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_desc` varchar(100) DEFAULT NULL,
  `enable_flag` tinyint(1) DEFAULT 1,
  `order_view` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `ot_skill` */

insert  into `ot_skill`(`skill_id`,`skill_desc`,`enable_flag`,`order_view`) values (1,'高齢技能者',1,1),(2,'女性技能者',1,2),(3,'一般技能者',1,3),(4,'DIY経験者',1,4);

/*Table structure for table `ot_terms_of_condition` */

DROP TABLE IF EXISTS `ot_terms_of_condition`;

CREATE TABLE `ot_terms_of_condition` (
  `condition_id` int(11) NOT NULL AUTO_INCREMENT,
  `condition_desc` varchar(1000) DEFAULT NULL,
  `condition_type` varchar(1000) DEFAULT NULL COMMENT 'R - Registration M - Membership',
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_terms_of_condition` */

/*Table structure for table `ot_user` */

DROP TABLE IF EXISTS `ot_user`;

CREATE TABLE `ot_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cond_accept_flag` tinyint(1) DEFAULT 0,
  `cond_accept_date` timestamp NULL DEFAULT NULL,
  `social_type` varchar(50) DEFAULT NULL COMMENT 'NULL - Otasuke registration FACEBOOK - Facebook GMAIL - Gmail',
  `user_type` varchar(50) DEFAULT NULL COMMENT 'CUSTOMER - Customer CREATOR - Creator',
  `gender` varchar(2) DEFAULT NULL COMMENT 'M - Male F- Female',
  `phone` varchar(50) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `work_place` varchar(200) DEFAULT NULL,
  `years_of_experience` smallint(6) DEFAULT NULL,
  `user_category` varchar(2) DEFAULT 'I' COMMENT 'I - Individual B - Business',
  `self_intro` varchar(500) DEFAULT NULL,
  `skill_id` int(11) DEFAULT NULL,
  `mem_accept_flag` tinyint(1) DEFAULT NULL COMMENT 'Membership flag for creator',
  `mem_accept_date` timestamp NULL DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `profile_image_id` int(11) DEFAULT NULL,
  `referral_info` varchar(200) DEFAULT NULL,
  `overal_rate` float DEFAULT NULL,
  `prefecture_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `IXFK_ot_user_ot_company` (`company_id`),
  KEY `IXFK_ot_user_ot_prefecture` (`prefecture_id`),
  KEY `IXFK_ot_user_ot_skill` (`skill_id`),
  CONSTRAINT `FK_ot_user_ot_company` FOREIGN KEY (`company_id`) REFERENCES `ot_company` (`company_id`),
  CONSTRAINT `FK_ot_user_ot_prefecture` FOREIGN KEY (`prefecture_id`) REFERENCES `ot_prefecture` (`prefecture_id`),
  CONSTRAINT `FK_ot_user_ot_skill_02` FOREIGN KEY (`skill_id`) REFERENCES `ot_skill` (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `ot_user` */

insert  into `ot_user`(`user_id`,`username`,`password`,`email`,`cond_accept_flag`,`cond_accept_date`,`social_type`,`user_type`,`gender`,`phone`,`dob`,`address`,`work_place`,`years_of_experience`,`user_category`,`self_intro`,`skill_id`,`mem_accept_flag`,`mem_accept_date`,`created_date`,`updated_date`,`company_id`,`profile_image_id`,`referral_info`,`overal_rate`,`prefecture_id`) values (1,'test','$2a$10$DX03seEb8ECvZAvLZjCj8OD0Z/7ftjCCvZVcIn1F35/p56cZZOqSS','user@yahoo.com',NULL,NULL,NULL,'CREATOR','M','88778877','2000-07-31','Takatsu','MiD',3,'I','Thank you',1,NULL,NULL,'2021-03-30 20:20:36',NULL,1,0,'Referral info',NULL,1);

/*Table structure for table `ot_user_occupation` */

DROP TABLE IF EXISTS `ot_user_occupation`;

CREATE TABLE `ot_user_occupation` (
  `user_occupation_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `occupation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_occupation_id`),
  KEY `IXFK_ot_user_occupation_ot_occupation` (`occupation_id`),
  KEY `IXFK_ot_user_occupation_ot_user` (`user_id`),
  CONSTRAINT `FK_ot_user_occupation_ot_occupation` FOREIGN KEY (`occupation_id`) REFERENCES `ot_occupation` (`occupation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ot_user_occupation_ot_user` FOREIGN KEY (`user_id`) REFERENCES `ot_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ot_user_occupation` */

insert  into `ot_user_occupation`(`user_occupation_id`,`user_id`,`occupation_id`) values (1,1,1),(2,1,2);

/*Table structure for table `ot_work` */

DROP TABLE IF EXISTS `ot_work`;

CREATE TABLE `ot_work` (
  `work_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image_count` smallint(6) DEFAULT NULL,
  `urgency_type` varchar(50) DEFAULT NULL COMMENT 'H - Hurry U - Usual NH - Not hurry',
  `prefecture_id` int(11) DEFAULT NULL,
  `municipality` varchar(200) DEFAULT NULL,
  `floor_plan_id` int(11) DEFAULT NULL,
  `building_age` year(4) DEFAULT NULL,
  `building_type_id` int(11) DEFAULT NULL,
  `send_type` varchar(2) DEFAULT NULL COMMENT 'A - All S - Special company',
  `company_id` int(11) DEFAULT NULL,
  `work_comment` varchar(500) DEFAULT NULL,
  `work_status` varchar(50) DEFAULT NULL COMMENT 'DRAFT - Draft&Sketch (just calculated) OPEN - Open NEGOTIATIONS - During negotiations APPROVED - Approved CANCELED - Canceled COMPLETED - Completed REJECT - Reject',
  `parent_id` int(11) DEFAULT NULL COMMENT 'work id of sketch (it for separted by category) ',
  `work_number` varchar(50) DEFAULT NULL,
  `available_day_start` date DEFAULT NULL,
  `available_day_end` date DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `progress` smallint(6) DEFAULT NULL,
  `plan_start_date` date DEFAULT NULL,
  `plan_end_date` date DEFAULT NULL,
  `customer_paid_flag` tinyint(1) DEFAULT NULL,
  `customer_paid_date` timestamp NULL DEFAULT NULL,
  `customer_paid_amount` double(10,2) DEFAULT NULL,
  `fee_paid_flag` tinyint(1) DEFAULT NULL,
  `fee_paid_date` timestamp NULL DEFAULT NULL,
  `fee_paid_amount` double(10,2) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `real_end_date` datetime DEFAULT NULL COMMENT 'Completed date time',
  `percent_of_fee` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`work_id`),
  KEY `IXFK_ot_work_ot_building_type` (`building_type_id`),
  KEY `IXFK_ot_work_ot_company` (`company_id`),
  KEY `IXFK_ot_work_ot_floor_plan` (`floor_plan_id`),
  KEY `IXFK_ot_work_ot_prefecture` (`prefecture_id`),
  KEY `IXFK_ot_work_ot_user` (`customer_id`),
  KEY `IXFK_ot_work_ot_user_02` (`creator_id`),
  KEY `IXFK_ot_work_ot_work_category` (`category_id`),
  CONSTRAINT `FK_ot_work_ot_building_type` FOREIGN KEY (`building_type_id`) REFERENCES `ot_building_type` (`building_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ot_work_ot_company` FOREIGN KEY (`company_id`) REFERENCES `ot_company` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ot_work_ot_floor_plan` FOREIGN KEY (`floor_plan_id`) REFERENCES `ot_floor_plan` (`floor_plan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ot_work_ot_prefecture` FOREIGN KEY (`prefecture_id`) REFERENCES `ot_prefecture` (`prefecture_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ot_work_ot_user` FOREIGN KEY (`customer_id`) REFERENCES `ot_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ot_work_ot_user_02` FOREIGN KEY (`creator_id`) REFERENCES `ot_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ot_work_ot_work_category` FOREIGN KEY (`category_id`) REFERENCES `ot_work_category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `ot_work` */

insert  into `ot_work`(`work_id`,`title`,`customer_id`,`category_id`,`image_count`,`urgency_type`,`prefecture_id`,`municipality`,`floor_plan_id`,`building_age`,`building_type_id`,`send_type`,`company_id`,`work_comment`,`work_status`,`parent_id`,`work_number`,`available_day_start`,`available_day_end`,`creator_id`,`amount`,`progress`,`plan_start_date`,`plan_end_date`,`customer_paid_flag`,`customer_paid_date`,`customer_paid_amount`,`fee_paid_flag`,`fee_paid_date`,`fee_paid_amount`,`postal_code`,`real_end_date`,`percent_of_fee`) values (2,'Work 1',NULL,1,NULL,'H',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'OPEN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Work 1',NULL,2,NULL,'U',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'OPEN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Work 5',NULL,3,NULL,'NH',5,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'OPEN',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'Work 8',NULL,4,NULL,'NH',4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'NEGOTIATIONS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Work 11',NULL,5,NULL,'U',3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'NEGOTIATIONS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Work 20',NULL,6,NULL,'H',10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'APPROVED',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Work 22',NULL,2,NULL,'U',11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'APPROVED',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Work 30',NULL,4,NULL,'U',15,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'COMPLETED',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'Work 33',NULL,6,NULL,'H',16,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'COMPLETED',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'Work 55',NULL,3,NULL,'NH',17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'NEGOTIATIONS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `ot_work_category` */

DROP TABLE IF EXISTS `ot_work_category`;

CREATE TABLE `ot_work_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_desc` varchar(200) DEFAULT NULL,
  `enable_flag` tinyint(1) DEFAULT 1,
  `order_view` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Wall, ceiling ..';

/*Data for the table `ot_work_category` */

insert  into `ot_work_category`(`category_id`,`category_desc`,`enable_flag`,`order_view`) values (1,'壁',1,1),(2,'ウィンドウ',1,2),(3,'ドア',1,3),(4,'床',1,4),(5,'天井',1,5),(6,'他',1,6);

/*Table structure for table `ot_work_deal` */

DROP TABLE IF EXISTS `ot_work_deal`;

CREATE TABLE `ot_work_deal` (
  `deal_id` int(11) NOT NULL AUTO_INCREMENT,
  `work_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `proposal_amount` double(10,2) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `deal_status` varchar(50) DEFAULT NULL COMMENT 'REQUESTED NEGOTIATIONS APPROVED COMPLETED',
  PRIMARY KEY (`deal_id`),
  KEY `IXFK_ot_work_deal_ot_work` (`work_id`),
  CONSTRAINT `FK_ot_work_deal_ot_work` FOREIGN KEY (`work_id`) REFERENCES `ot_work` (`work_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ot_work_deal` */

/*Table structure for table `ot_work_image` */

DROP TABLE IF EXISTS `ot_work_image`;

CREATE TABLE `ot_work_image` (
  `work_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `work_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `distance_type` varchar(2) DEFAULT 'F' COMMENT 'N - Near F- Far',
  `calculated_scratch_json` varchar(1000) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image_type` varchar(2) DEFAULT 'P' COMMENT 'P - Previous A - After',
  `created_user_id` int(11) DEFAULT NULL,
  `created_date` timestamp(4) NULL DEFAULT NULL,
  `manual_scratch_json` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`work_image_id`),
  KEY `IXFK_ot_work_image_ot_work` (`work_id`),
  CONSTRAINT `FK_ot_work_image_ot_work` FOREIGN KEY (`work_id`) REFERENCES `ot_work` (`work_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `ot_work_image` */

insert  into `ot_work_image`(`work_image_id`,`work_id`,`image_id`,`distance_type`,`calculated_scratch_json`,`category_id`,`image_type`,`created_user_id`,`created_date`,`manual_scratch_json`) values (1,8,2,'F',NULL,NULL,'P',NULL,NULL,NULL),(2,10,3,'F',NULL,NULL,'P',NULL,NULL,NULL),(3,5,4,'F',NULL,NULL,'P',NULL,NULL,NULL),(4,6,5,'F',NULL,NULL,'P',NULL,NULL,NULL),(5,8,3,'F',NULL,NULL,'P',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

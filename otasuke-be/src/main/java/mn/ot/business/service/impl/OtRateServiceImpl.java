/*
 * Created on 29 Mar 2021 ( Time 20:37:32 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mn.ot.bean.OtRate;
import mn.ot.bean.jpa.OtRateEntity;
import mn.ot.business.service.OtRateService;
import mn.ot.business.service.mapping.OtRateServiceMapper;
import mn.ot.data.repository.jpa.OtRateJpaRepository;

/**
 * Implementation of OtRateService
 */
@Component
@Transactional
public class OtRateServiceImpl implements OtRateService {

	@Resource
	private OtRateJpaRepository otRateJpaRepository;

	@Resource
	private OtRateServiceMapper otRateServiceMapper;
	
	@Override
	public OtRate findById(Integer rateId) {
		OtRateEntity otRateEntity = otRateJpaRepository.findById(rateId).orElse(null);
		return otRateServiceMapper.mapOtRateEntityToOtRate(otRateEntity);
	}

	@Override
	public List<OtRate> findAll() {
		Iterable<OtRateEntity> entities = otRateJpaRepository.findAll();
		List<OtRate> beans = new ArrayList<OtRate>();
		for(OtRateEntity otRateEntity : entities) {
			beans.add(otRateServiceMapper.mapOtRateEntityToOtRate(otRateEntity));
		}
		return beans;
	}

	@Override
	public OtRate save(OtRate otRate) {
		return update(otRate) ;
	}

	@Override
	public OtRate create(OtRate otRate) {
		OtRateEntity otRateEntity = otRateJpaRepository.findById(otRate.getRateId()).orElse(null);
		if( otRateEntity != null ) {
			throw new IllegalStateException("already.exists");
		}
		otRateEntity = new OtRateEntity();
		otRateServiceMapper.mapOtRateToOtRateEntity(otRate, otRateEntity);
		OtRateEntity otRateEntitySaved = otRateJpaRepository.save(otRateEntity);
		return otRateServiceMapper.mapOtRateEntityToOtRate(otRateEntitySaved);
	}

	@Override
	public OtRate update(OtRate otRate) {
		OtRateEntity otRateEntity = otRateJpaRepository.findById(otRate.getRateId()).orElse(null);
		otRateServiceMapper.mapOtRateToOtRateEntity(otRate, otRateEntity);
		OtRateEntity otRateEntitySaved = otRateJpaRepository.save(otRateEntity);
		return otRateServiceMapper.mapOtRateEntityToOtRate(otRateEntitySaved);
	}

	@Override
	public void delete(Integer rateId) {
		otRateJpaRepository.deleteById(rateId);
	}

	public OtRateJpaRepository getOtRateJpaRepository() {
		return otRateJpaRepository;
	}

	public void setOtRateJpaRepository(OtRateJpaRepository otRateJpaRepository) {
		this.otRateJpaRepository = otRateJpaRepository;
	}

	public OtRateServiceMapper getOtRateServiceMapper() {
		return otRateServiceMapper;
	}

	public void setOtRateServiceMapper(OtRateServiceMapper otRateServiceMapper) {
		this.otRateServiceMapper = otRateServiceMapper;
	}

}

/*
 * Created on 29 Mar 2021 ( Time 20:37:33 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import mn.ot.bean.OtRate;
import mn.ot.bean.jpa.OtRateEntity;
import mn.ot.bean.jpa.OtUserEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class OtRateServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public OtRateServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'OtRateEntity' to 'OtRate'
	 * @param otRateEntity
	 */
	public OtRate mapOtRateEntityToOtRate(OtRateEntity otRateEntity) {
		if(otRateEntity == null) {
			return null;
		}

		//--- Generic mapping 
		OtRate otRate = map(otRateEntity, OtRate.class);

		//--- Link mapping ( link to OtUser )
		if(otRateEntity.getOtUser() != null) {
			otRate.setUserId(otRateEntity.getOtUser().getUserId());
		}
		return otRate;
	}
	
	/**
	 * Mapping from 'OtRate' to 'OtRateEntity'
	 * @param otRate
	 * @param otRateEntity
	 */
	public void mapOtRateToOtRateEntity(OtRate otRate, OtRateEntity otRateEntity) {
		if(otRate == null) {
			return;
		}

		//--- Generic mapping 
		map(otRate, otRateEntity);

		//--- Link mapping ( link : otRate )
		if( hasLinkToOtUser(otRate) ) {
			OtUserEntity otUser1 = new OtUserEntity();
			otUser1.setUserId( otRate.getUserId() );
			otRateEntity.setOtUser( otUser1 );
		} else {
			otRateEntity.setOtUser( null );
		}

	}
	
	/**
	 * Verify that OtUser id is valid.
	 * @param OtUser OtUser
	 * @return boolean
	 */
	private boolean hasLinkToOtUser(OtRate otRate) {
		if(otRate.getUserId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}
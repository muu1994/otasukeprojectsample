/*
 * Created on 29 Mar 2021 ( Time 20:37:32 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mn.ot.bean.OtDealComment;
import mn.ot.bean.jpa.OtDealCommentEntity;
import mn.ot.business.service.OtDealCommentService;
import mn.ot.business.service.mapping.OtDealCommentServiceMapper;
import mn.ot.data.repository.jpa.OtDealCommentJpaRepository;

/**
 * Implementation of OtDealCommentService
 */
@Component
@Transactional
public class OtDealCommentServiceImpl implements OtDealCommentService {

	@Resource
	private OtDealCommentJpaRepository otDealCommentJpaRepository;

	@Resource
	private OtDealCommentServiceMapper otDealCommentServiceMapper;
	
	@Override
	public OtDealComment findById(Integer commentId) {
		OtDealCommentEntity otDealCommentEntity = otDealCommentJpaRepository.findById(commentId).orElse(null);
		return otDealCommentServiceMapper.mapOtDealCommentEntityToOtDealComment(otDealCommentEntity);
	}

	@Override
	public List<OtDealComment> findAll() {
		Iterable<OtDealCommentEntity> entities = otDealCommentJpaRepository.findAll();
		List<OtDealComment> beans = new ArrayList<OtDealComment>();
		for(OtDealCommentEntity otDealCommentEntity : entities) {
			beans.add(otDealCommentServiceMapper.mapOtDealCommentEntityToOtDealComment(otDealCommentEntity));
		}
		return beans;
	}

	@Override
	public OtDealComment save(OtDealComment otDealComment) {
		return update(otDealComment) ;
	}

	@Override
	public OtDealComment create(OtDealComment otDealComment) {
		OtDealCommentEntity otDealCommentEntity = otDealCommentJpaRepository.findById(otDealComment.getCommentId()).orElse(null);
		if( otDealCommentEntity != null ) {
			throw new IllegalStateException("already.exists");
		}
		otDealCommentEntity = new OtDealCommentEntity();
		otDealCommentServiceMapper.mapOtDealCommentToOtDealCommentEntity(otDealComment, otDealCommentEntity);
		OtDealCommentEntity otDealCommentEntitySaved = otDealCommentJpaRepository.save(otDealCommentEntity);
		return otDealCommentServiceMapper.mapOtDealCommentEntityToOtDealComment(otDealCommentEntitySaved);
	}

	@Override
	public OtDealComment update(OtDealComment otDealComment) {
		OtDealCommentEntity otDealCommentEntity = otDealCommentJpaRepository.findById(otDealComment.getCommentId()).orElse(null);
		otDealCommentServiceMapper.mapOtDealCommentToOtDealCommentEntity(otDealComment, otDealCommentEntity);
		OtDealCommentEntity otDealCommentEntitySaved = otDealCommentJpaRepository.save(otDealCommentEntity);
		return otDealCommentServiceMapper.mapOtDealCommentEntityToOtDealComment(otDealCommentEntitySaved);
	}

	@Override
	public void delete(Integer commentId) {
		otDealCommentJpaRepository.deleteById(commentId);
	}

	public OtDealCommentJpaRepository getOtDealCommentJpaRepository() {
		return otDealCommentJpaRepository;
	}

	public void setOtDealCommentJpaRepository(OtDealCommentJpaRepository otDealCommentJpaRepository) {
		this.otDealCommentJpaRepository = otDealCommentJpaRepository;
	}

	public OtDealCommentServiceMapper getOtDealCommentServiceMapper() {
		return otDealCommentServiceMapper;
	}

	public void setOtDealCommentServiceMapper(OtDealCommentServiceMapper otDealCommentServiceMapper) {
		this.otDealCommentServiceMapper = otDealCommentServiceMapper;
	}

}

/*
 * Created on 29 Mar 2021 ( Time 20:37:32 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import mn.ot.bean.OtBuildingType;
import mn.ot.bean.jpa.OtBuildingTypeEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class OtBuildingTypeServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public OtBuildingTypeServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'OtBuildingTypeEntity' to 'OtBuildingType'
	 * @param otBuildingTypeEntity
	 */
	public OtBuildingType mapOtBuildingTypeEntityToOtBuildingType(OtBuildingTypeEntity otBuildingTypeEntity) {
		if(otBuildingTypeEntity == null) {
			return null;
		}

		//--- Generic mapping 
		OtBuildingType otBuildingType = map(otBuildingTypeEntity, OtBuildingType.class);

		return otBuildingType;
	}
	
	/**
	 * Mapping from 'OtBuildingType' to 'OtBuildingTypeEntity'
	 * @param otBuildingType
	 * @param otBuildingTypeEntity
	 */
	public void mapOtBuildingTypeToOtBuildingTypeEntity(OtBuildingType otBuildingType, OtBuildingTypeEntity otBuildingTypeEntity) {
		if(otBuildingType == null) {
			return;
		}

		//--- Generic mapping 
		map(otBuildingType, otBuildingTypeEntity);

	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}
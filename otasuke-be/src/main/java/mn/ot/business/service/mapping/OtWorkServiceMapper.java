/*
 * Created on 29 Mar 2021 ( Time 20:37:33 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import mn.ot.bean.OtWork;
import mn.ot.bean.jpa.OtBuildingTypeEntity;
import mn.ot.bean.jpa.OtCompanyEntity;
import mn.ot.bean.jpa.OtFloorPlanEntity;
import mn.ot.bean.jpa.OtPrefectureEntity;
import mn.ot.bean.jpa.OtUserEntity;
import mn.ot.bean.jpa.OtWorkCategoryEntity;
import mn.ot.bean.jpa.OtWorkEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class OtWorkServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public OtWorkServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'OtWorkEntity' to 'OtWork'
	 * @param otWorkEntity
	 */
	public OtWork mapOtWorkEntityToOtWork(OtWorkEntity otWorkEntity) {
		if(otWorkEntity == null) {
			return null;
		}

		//--- Generic mapping 
		OtWork otWork = map(otWorkEntity, OtWork.class);

		//--- Link mapping ( link to OtBuildingType )
		if(otWorkEntity.getOtBuildingType() != null) {
			otWork.setBuildingTypeId(otWorkEntity.getOtBuildingType().getBuildingTypeId());
		}
		//--- Link mapping ( link to OtUser )
		if(otWorkEntity.getOtUserCreator() != null) {
			otWork.setCreatorId(otWorkEntity.getOtUserCreator().getUserId());
		}
		//--- Link mapping ( link to OtUser )
		if(otWorkEntity.getOtUserCustomer() != null) {
			otWork.setCustomerId(otWorkEntity.getOtUserCustomer().getUserId());
		}
		//--- Link mapping ( link to OtWorkCategory )
		if(otWorkEntity.getOtWorkCategory() != null) {
			otWork.setCategoryId(otWorkEntity.getOtWorkCategory().getCategoryId());
		}
		//--- Link mapping ( link to OtFloorPlan )
		if(otWorkEntity.getOtFloorPlan() != null) {
			otWork.setFloorPlanId(otWorkEntity.getOtFloorPlan().getFloorPlanId());
		}
		//--- Link mapping ( link to OtPrefecture )
		if(otWorkEntity.getOtPrefecture() != null) {
			otWork.setPrefectureId(otWorkEntity.getOtPrefecture().getPrefectureId());
		}
		//--- Link mapping ( link to OtCompany )
		if(otWorkEntity.getOtCompany() != null) {
			otWork.setCompanyId(otWorkEntity.getOtCompany().getCompanyId());
		}
		return otWork;
	}
	
	/**
	 * Mapping from 'OtWork' to 'OtWorkEntity'
	 * @param otWork
	 * @param otWorkEntity
	 */
	public void mapOtWorkToOtWorkEntity(OtWork otWork, OtWorkEntity otWorkEntity) {
		if(otWork == null) {
			return;
		}

		//--- Generic mapping 
		map(otWork, otWorkEntity);

		//--- Link mapping ( link : otWork )
		if( hasLinkToOtBuildingType(otWork) ) {
			OtBuildingTypeEntity otBuildingType1 = new OtBuildingTypeEntity();
			otBuildingType1.setBuildingTypeId( otWork.getBuildingTypeId() );
			otWorkEntity.setOtBuildingType( otBuildingType1 );
		} else {
			otWorkEntity.setOtBuildingType( null );
		}

		//--- Link mapping ( link : otWork )
		if( hasLinkToCreatorId(otWork) ) {
			OtUserEntity otUserCreator = new OtUserEntity();
			otUserCreator.setUserId( otWork.getCreatorId() );
			otWorkEntity.setOtUserCreator( otUserCreator );
		} else {
			otWorkEntity.setOtUserCreator( null );
		}

		//--- Link mapping ( link : otWork )
		if( hasLinkToCustomerId(otWork) ) {
			OtUserEntity otUserCustomer = new OtUserEntity();
			otUserCustomer.setUserId( otWork.getCustomerId() );
			otWorkEntity.setOtUserCustomer( otUserCustomer );
		} else {
			otWorkEntity.setOtUserCustomer( null );
		}

		//--- Link mapping ( link : otWork )
		if( hasLinkToOtWorkCategory(otWork) ) {
			OtWorkCategoryEntity otWorkCategory4 = new OtWorkCategoryEntity();
			otWorkCategory4.setCategoryId( otWork.getCategoryId() );
			otWorkEntity.setOtWorkCategory( otWorkCategory4 );
		} else {
			otWorkEntity.setOtWorkCategory( null );
		}

		//--- Link mapping ( link : otWork )
		if( hasLinkToOtFloorPlan(otWork) ) {
			OtFloorPlanEntity otFloorPlan5 = new OtFloorPlanEntity();
			otFloorPlan5.setFloorPlanId( otWork.getFloorPlanId() );
			otWorkEntity.setOtFloorPlan( otFloorPlan5 );
		} else {
			otWorkEntity.setOtFloorPlan( null );
		}

		//--- Link mapping ( link : otWork )
		if( hasLinkToOtPrefecture(otWork) ) {
			OtPrefectureEntity otPrefecture6 = new OtPrefectureEntity();
			otPrefecture6.setPrefectureId( otWork.getPrefectureId() );
			otWorkEntity.setOtPrefecture( otPrefecture6 );
		} else {
			otWorkEntity.setOtPrefecture( null );
		}

		//--- Link mapping ( link : otWork )
		if( hasLinkToOtCompany(otWork) ) {
			OtCompanyEntity otCompany7 = new OtCompanyEntity();
			otCompany7.setCompanyId( otWork.getCompanyId() );
			otWorkEntity.setOtCompany( otCompany7 );
		} else {
			otWorkEntity.setOtCompany( null );
		}

	}
	
	/**
	 * Verify that OtBuildingType id is valid.
	 * @param OtBuildingType OtBuildingType
	 * @return boolean
	 */
	private boolean hasLinkToOtBuildingType(OtWork otWork) {
		if(otWork.getBuildingTypeId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * Verify that OtUser id is valid.
	 * @param OtUser OtUser
	 * @return boolean
	 */
	private boolean hasLinkToCreatorId(OtWork otWork) {
		if(otWork.getCreatorId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * Verify that OtUser id is valid.
	 * @param OtUser OtUser
	 * @return boolean
	 */
	private boolean hasLinkToCustomerId(OtWork otWork) {
		if(otWork.getCustomerId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * Verify that OtWorkCategory id is valid.
	 * @param OtWorkCategory OtWorkCategory
	 * @return boolean
	 */
	private boolean hasLinkToOtWorkCategory(OtWork otWork) {
		if(otWork.getCategoryId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * Verify that OtFloorPlan id is valid.
	 * @param OtFloorPlan OtFloorPlan
	 * @return boolean
	 */
	private boolean hasLinkToOtFloorPlan(OtWork otWork) {
		if(otWork.getFloorPlanId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * Verify that OtPrefecture id is valid.
	 * @param OtPrefecture OtPrefecture
	 * @return boolean
	 */
	private boolean hasLinkToOtPrefecture(OtWork otWork) {
		if(otWork.getPrefectureId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * Verify that OtCompany id is valid.
	 * @param OtCompany OtCompany
	 * @return boolean
	 */
	private boolean hasLinkToOtCompany(OtWork otWork) {
		if(otWork.getCompanyId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}
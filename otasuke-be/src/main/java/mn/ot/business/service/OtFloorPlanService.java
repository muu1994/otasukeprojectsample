/*
 * Created on 29 Mar 2021 ( Time 20:37:32 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service;

import java.util.List;

import mn.ot.bean.OtFloorPlan;

/**
 * Business Service Interface for entity OtFloorPlan.
 */
public interface OtFloorPlanService { 

	/**
	 * Loads an entity from the database using its Primary Key
	 * @param floorPlanId
	 * @return entity
	 */
	OtFloorPlan findById( Integer floorPlanId  ) ;

	/**
	 * Loads all entities.
	 * @return all entities
	 */
	List<OtFloorPlan> findAll();

	/**
	 * Saves the given entity in the database (create or update)
	 * @param entity
	 * @return entity
	 */
	OtFloorPlan save(OtFloorPlan entity);

	/**
	 * Updates the given entity in the database
	 * @param entity
	 * @return
	 */
	OtFloorPlan update(OtFloorPlan entity);

	/**
	 * Creates the given entity in the database
	 * @param entity
	 * @return
	 */
	OtFloorPlan create(OtFloorPlan entity);

	/**
	 * Deletes an entity using its Primary Key
	 * @param floorPlanId
	 */
	void delete( Integer floorPlanId );


}

/*
 * Created on 29 Mar 2021 ( Time 20:37:32 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import mn.ot.bean.OtPublicHoliday;
import mn.ot.bean.jpa.OtPublicHolidayEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class OtPublicHolidayServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public OtPublicHolidayServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'OtPublicHolidayEntity' to 'OtPublicHoliday'
	 * @param otPublicHolidayEntity
	 */
	public OtPublicHoliday mapOtPublicHolidayEntityToOtPublicHoliday(OtPublicHolidayEntity otPublicHolidayEntity) {
		if(otPublicHolidayEntity == null) {
			return null;
		}

		//--- Generic mapping 
		OtPublicHoliday otPublicHoliday = map(otPublicHolidayEntity, OtPublicHoliday.class);

		return otPublicHoliday;
	}
	
	/**
	 * Mapping from 'OtPublicHoliday' to 'OtPublicHolidayEntity'
	 * @param otPublicHoliday
	 * @param otPublicHolidayEntity
	 */
	public void mapOtPublicHolidayToOtPublicHolidayEntity(OtPublicHoliday otPublicHoliday, OtPublicHolidayEntity otPublicHolidayEntity) {
		if(otPublicHoliday == null) {
			return;
		}

		//--- Generic mapping 
		map(otPublicHoliday, otPublicHolidayEntity);

	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}
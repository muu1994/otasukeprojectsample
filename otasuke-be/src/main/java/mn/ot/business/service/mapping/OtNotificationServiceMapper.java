/*
 * Created on 29 Mar 2021 ( Time 20:37:32 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import mn.ot.bean.OtNotification;
import mn.ot.bean.jpa.OtNotificationEntity;
import mn.ot.bean.jpa.OtWorkEntity;
import mn.ot.bean.jpa.OtUserEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class OtNotificationServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;
	
	/**
	 * Constructor.
	 */
	public OtNotificationServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'OtNotificationEntity' to 'OtNotification'
	 * @param otNotificationEntity
	 */
	public OtNotification mapOtNotificationEntityToOtNotification(OtNotificationEntity otNotificationEntity) {
		if(otNotificationEntity == null) {
			return null;
		}

		//--- Generic mapping 
		OtNotification otNotification = map(otNotificationEntity, OtNotification.class);

		//--- Link mapping ( link to OtWork )
		if(otNotificationEntity.getOtWork() != null) {
			otNotification.setWorkId(otNotificationEntity.getOtWork().getWorkId());
		}
		//--- Link mapping ( link to OtUser )
		if(otNotificationEntity.getOtUser() != null) {
			otNotification.setUserId(otNotificationEntity.getOtUser().getUserId());
		}
		return otNotification;
	}
	
	/**
	 * Mapping from 'OtNotification' to 'OtNotificationEntity'
	 * @param otNotification
	 * @param otNotificationEntity
	 */
	public void mapOtNotificationToOtNotificationEntity(OtNotification otNotification, OtNotificationEntity otNotificationEntity) {
		if(otNotification == null) {
			return;
		}

		//--- Generic mapping 
		map(otNotification, otNotificationEntity);

		//--- Link mapping ( link : otNotification )
		if( hasLinkToOtWork(otNotification) ) {
			OtWorkEntity otWork1 = new OtWorkEntity();
			otWork1.setWorkId( otNotification.getWorkId() );
			otNotificationEntity.setOtWork( otWork1 );
		} else {
			otNotificationEntity.setOtWork( null );
		}

		//--- Link mapping ( link : otNotification )
		if( hasLinkToOtUser(otNotification) ) {
			OtUserEntity otUser2 = new OtUserEntity();
			otUser2.setUserId( otNotification.getUserId() );
			otNotificationEntity.setOtUser( otUser2 );
		} else {
			otNotificationEntity.setOtUser( null );
		}

	}
	
	/**
	 * Verify that OtWork id is valid.
	 * @param OtWork OtWork
	 * @return boolean
	 */
	private boolean hasLinkToOtWork(OtNotification otNotification) {
		if(otNotification.getWorkId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * Verify that OtUser id is valid.
	 * @param OtUser OtUser
	 * @return boolean
	 */
	private boolean hasLinkToOtUser(OtNotification otNotification) {
		if(otNotification.getUserId() != null) {
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}
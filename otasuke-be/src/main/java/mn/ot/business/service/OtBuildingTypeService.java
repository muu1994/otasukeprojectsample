/*
 * Created on 29 Mar 2021 ( Time 20:37:32 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.business.service;

import java.util.List;

import mn.ot.bean.OtBuildingType;

/**
 * Business Service Interface for entity OtBuildingType.
 */
public interface OtBuildingTypeService { 

	/**
	 * Loads an entity from the database using its Primary Key
	 * @param buildingTypeId
	 * @return entity
	 */
	OtBuildingType findById( Integer buildingTypeId  ) ;

	/**
	 * Loads all entities.
	 * @return all entities
	 */
	List<OtBuildingType> findAll();

	/**
	 * Saves the given entity in the database (create or update)
	 * @param entity
	 * @return entity
	 */
	OtBuildingType save(OtBuildingType entity);

	/**
	 * Updates the given entity in the database
	 * @param entity
	 * @return
	 */
	OtBuildingType update(OtBuildingType entity);

	/**
	 * Creates the given entity in the database
	 * @param entity
	 * @return
	 */
	OtBuildingType create(OtBuildingType entity);

	/**
	 * Deletes an entity using its Primary Key
	 * @param buildingTypeId
	 */
	void delete( Integer buildingTypeId );


}

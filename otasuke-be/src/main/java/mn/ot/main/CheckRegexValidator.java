package mn.ot.main;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
/**
 * Regex validator
 * @author GANZORIG KhashErdene
 * @date 2021.03.29
 */
public class CheckRegexValidator implements ConstraintValidator<CheckRegexPattern, String> {

    private String pattern;

    @Override
    public void initialize(CheckRegexPattern constraintAnnotation) {
        this.pattern = constraintAnnotation.pattern();
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext constraintContext) {
        if ( object == null ) {
            return true;
        }
        return object.matches(pattern);
    }
}
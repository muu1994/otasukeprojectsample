package mn.ot.main;
/**
 * Validation result
 * @author GANZORIG KhashErdene
 * @date 2021.03.29
 */
public class FieldErrorDto {
	
	private String field;
    private String message;
 
    public FieldErrorDto(String field, String message) {
        this.field = field;
        this.message = message;
    }

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

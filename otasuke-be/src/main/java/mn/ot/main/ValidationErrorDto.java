package mn.ot.main;

import java.util.ArrayList;
import java.util.List;

import mn.ot.dto.ResponseDto;

/**
 * Validation error DTO
 * @author GANZORIG KhashErdene
 * @date 2021.03.29
 */
public class ValidationErrorDto extends ResponseDto {
	
	private List<FieldErrorDto> fieldErrors = new ArrayList<FieldErrorDto>();
	 
    public ValidationErrorDto() {
    	setStatus(ResponseConst.ERR_VALIDATION);
    	setMessage(ResponseConst.ERR_VALIDATION_TEXT);
    }
 
    public void addFieldError(String path, String message) {
        FieldErrorDto error = new FieldErrorDto(path, message);
        fieldErrors.add(error);
    }

	public List<FieldErrorDto> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldErrorDto> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
}

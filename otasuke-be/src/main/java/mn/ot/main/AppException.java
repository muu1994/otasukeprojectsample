package mn.ot.main;

import mn.ot.dto.ResponseDto;
/**
 * Application exception
 * @author GANZORIG KhashErdene
 * @date 2021.03.29
 */
public class AppException extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private ResponseDto responseDto;

	public AppException(ResponseDto responseDto) {
		this.responseDto = responseDto;
	}

	public String toString() {
		return responseDto.getStatus() + ": " + responseDto.getMessage();
	}

	public ResponseDto getResponseDto() {
		return responseDto;
	}

	public void setResponseDto(ResponseDto responseDto) {
		this.responseDto = responseDto;
	}
	
	
}

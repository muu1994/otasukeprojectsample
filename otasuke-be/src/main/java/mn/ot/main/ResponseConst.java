package mn.ot.main;
/**
 * Response constant
 * @author GANZORIG KhashErdene
 * @date 2021.03.29
 */
public class ResponseConst {
	public static String SUCCESS = "000";
	public static String ERR_SYS = "100";
	
	public static String ERR_VALIDATION = "008";
	public static String ERR_VALIDATION_TEXT = "Мэдээлэл буруу байна";
	public static String ERROR_INVALID_CREDENTIALS = "009";
	public static String ERROR_USER_NOT_CONFIRMED = "010";
	public static String ERROR_COULDNT_SEND_SMS = "011";
	public static String ERROR_USER_PHONE_DUPLICATE = "012";
	public static String ERROR_CREATOR_OCCUPATION_IS_NULL = "013";
	public static String ERROR_USER_PASSWORD_NEED_TO_ENTER = "014";
}

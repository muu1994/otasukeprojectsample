package mn.ot.main;

/**
 * Application constants
 * 
 * @author GANZORIG KhashErdene
 * @date 2021.03.29
 */
public class AppConst {

	public static final String FLAG_INACTIVE = "0";
	public static final String FLAG_ACTIVE = "1";
	
	public static final String USER_TYPE_CREATOR = "CREATOR";
	public static final String USER_TYPE_CUSTOMER = "CUSTOMER";
	
	public static final String USER_SOCIAL_TYPE_FB = "FACEBOOK";
	public static final String USER_SOCIAL_TYPE_GM = "GMAIL";
}

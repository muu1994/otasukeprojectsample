package mn.ot.security;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
/**
 * Authentication user
 * @author GANZORIG KhashErdene
 * @date 2019.07.19
 */
public class AuthUser extends User {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String phone;
	
	public AuthUser(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked, Integer id) {
		
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, AuthorityUtils.commaSeparatedStringToAuthorityList("USER"));
		this.id = id;
		this.phone = username;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}

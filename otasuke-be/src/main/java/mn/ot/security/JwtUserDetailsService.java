package mn.ot.security;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import mn.ot.bean.OtUser;
import mn.ot.business.service.OtUserService;
/**
 * Custom user details
 * @author GANZORIG KhashErdene
 * @date 2021.03.29
 */
@Service("userDetailsService")
public class JwtUserDetailsService implements UserDetailsService {

	@Resource
    private OtUserService otUserService;
	
	@Override
	public AuthUser loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		
		List<OtUser> userList = otUserService.findByUsername(userName);
		
		if(userList == null || userList.isEmpty()){
			throw new UsernameNotFoundException("NOT_FOUND");
		}
		
//		if(AppConst.FLAG_INACTIVE.equals(userList.get(0).getEnableFlag())){
//			throw new DisabledException("NOT_CONFIRM");
//		}
		
		AuthUser user = new AuthUser(userList.get(0).getPhone(), userList.get(0).getPassword(), true, true, true, true, userList.get(0).getUserId());
		return user;
	}
}

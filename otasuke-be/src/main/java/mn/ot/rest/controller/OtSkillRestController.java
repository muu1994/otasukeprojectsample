/*
 * Created on 29 Mar 2021 ( Time 20:36:47 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.rest.controller;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.ot.bean.OtOccupation;
import mn.ot.bean.OtSkill;
import mn.ot.business.service.OtSkillService;
import mn.ot.dto.ResponseDto;
import mn.ot.main.ResponseConst;

/**
 * Spring MVC controller for 'OtSkill' management.
 */
@RestController
public class OtSkillRestController extends AbstractRestController {

	@Resource
	private OtSkillService otSkillService;
	
	@RequestMapping( value="/otSkill",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ResponseDto findAll(HttpServletRequest httpServletRequest) {
		
		logger.info("findAll start");
		
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatus(ResponseConst.SUCCESS);
		Locale locale = RequestContextUtils.getLocale(httpServletRequest);
		
		try {
			List<OtSkill> otSkillList = otSkillService.findAll();
			responseDto.setEntity(otSkillList);
		} catch (Exception e) {
			logger.error("ERROR: ", e);
			responseDto.setStatus(ResponseConst.ERR_SYS);
		}
		
		setMessage(responseDto, locale);
		
		logger.info("findAll end");
		
		return responseDto;
	}

	@RequestMapping( value="/otSkill/{skillId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtSkill findById(@PathVariable("skillId") Integer skillId) {
		return otSkillService.findById(skillId);
	}
	
	@RequestMapping( value="/otSkill",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtSkill create(@RequestBody OtSkill otSkill) {
		return otSkillService.create(otSkill);
	}

	@RequestMapping( value="/otSkill/{skillId}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtSkill update(@PathVariable("skillId") Integer skillId, @RequestBody OtSkill otSkill) {
		return otSkillService.update(otSkill);
	}

	@RequestMapping( value="/otSkill/{skillId}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void delete(@PathVariable("skillId") Integer skillId) {
		otSkillService.delete(skillId);
	}
	
}

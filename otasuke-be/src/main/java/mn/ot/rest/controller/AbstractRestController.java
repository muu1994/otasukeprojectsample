package mn.ot.rest.controller;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import mn.ot.dto.ResponseDto;
import mn.ot.main.ValidationErrorDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.*;
/**
 * Abstract Rest Controller
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2019.07.04
 */
public abstract class AbstractRestController {
	@Resource
	private MessageSource messageSource;
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public void setMessage(ResponseDto responseDto, Locale locale){
		String responseText = getMessageSource().getMessage(responseDto.getStatus(), null, locale);
		responseDto.setMessage(responseText);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ValidationErrorDto processValidationError(MethodArgumentNotValidException ex) {
		
		logger.error("Process validation Error");
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
 
        return processFieldErrors(fieldErrors);
    }
	
	private ValidationErrorDto processFieldErrors(List<FieldError> fieldErrors) {
        ValidationErrorDto dto = new ValidationErrorDto();
 
        for (FieldError fieldError: fieldErrors) {
        	logger.error("Field: " + fieldError.getField());
        	logger.error("Rejected value: " + fieldError.getRejectedValue());
        	logger.error("Default message: " + fieldError.getDefaultMessage());
        	
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            dto.addFieldError(fieldError.getField(), localizedErrorMessage);
        }
        
        return dto;
    }
	
	private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale =  LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);
 
        //If the message was not found, return the most accurate field error code instead.
        //You can remove this check if you prefer to get the default error message.
        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }
 
        return localizedErrorMessage;
    }
}

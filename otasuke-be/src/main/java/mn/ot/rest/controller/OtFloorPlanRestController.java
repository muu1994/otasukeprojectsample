/*
 * Created on 29 Mar 2021 ( Time 20:36:45 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.rest.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.ot.bean.OtFloorPlan;
import mn.ot.business.service.OtFloorPlanService;

/**
 * Spring MVC controller for 'OtFloorPlan' management.
 */
@RestController
public class OtFloorPlanRestController extends AbstractRestController {

	@Resource
	private OtFloorPlanService otFloorPlanService;
	
	@RequestMapping( value="/otFloorPlan",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<OtFloorPlan> findAll() {
		return otFloorPlanService.findAll();
	}

	@RequestMapping( value="/otFloorPlan/{floorPlanId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtFloorPlan findById(@PathVariable("floorPlanId") Integer floorPlanId) {
		return otFloorPlanService.findById(floorPlanId);
	}
	
	@RequestMapping( value="/otFloorPlan",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtFloorPlan create(@RequestBody OtFloorPlan otFloorPlan) {
		return otFloorPlanService.create(otFloorPlan);
	}

	@RequestMapping( value="/otFloorPlan/{floorPlanId}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtFloorPlan update(@PathVariable("floorPlanId") Integer floorPlanId, @RequestBody OtFloorPlan otFloorPlan) {
		return otFloorPlanService.update(otFloorPlan);
	}

	@RequestMapping( value="/otFloorPlan/{floorPlanId}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void delete(@PathVariable("floorPlanId") Integer floorPlanId) {
		otFloorPlanService.delete(floorPlanId);
	}
	
}

/*
 * Created on 29 Mar 2021 ( Time 20:36:47 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.rest.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.ot.bean.OtTermsOfCondition;
import mn.ot.business.service.OtTermsOfConditionService;

/**
 * Spring MVC controller for 'OtTermsOfCondition' management.
 */
@RestController
public class OtTermsOfConditionRestController extends AbstractRestController {

	@Resource
	private OtTermsOfConditionService otTermsOfConditionService;
	
	@RequestMapping( value="/otTermsOfCondition",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<OtTermsOfCondition> findAll() {
		return otTermsOfConditionService.findAll();
	}

	@RequestMapping( value="/otTermsOfCondition/{conditionId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtTermsOfCondition findById(@PathVariable("conditionId") Integer conditionId) {
		return otTermsOfConditionService.findById(conditionId);
	}
	
	@RequestMapping( value="/otTermsOfCondition",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtTermsOfCondition create(@RequestBody OtTermsOfCondition otTermsOfCondition) {
		return otTermsOfConditionService.create(otTermsOfCondition);
	}

	@RequestMapping( value="/otTermsOfCondition/{conditionId}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtTermsOfCondition update(@PathVariable("conditionId") Integer conditionId, @RequestBody OtTermsOfCondition otTermsOfCondition) {
		return otTermsOfConditionService.update(otTermsOfCondition);
	}

	@RequestMapping( value="/otTermsOfCondition/{conditionId}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void delete(@PathVariable("conditionId") Integer conditionId) {
		otTermsOfConditionService.delete(conditionId);
	}
	
}

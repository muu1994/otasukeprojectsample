/*
 * Created on 29 Mar 2021 ( Time 20:36:45 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.rest.controller;

import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import mn.ot.bean.OtCompany;
import mn.ot.bean.OtSkill;
import mn.ot.business.service.OtCompanyService;
import mn.ot.dto.ResponseDto;
import mn.ot.main.ResponseConst;

/**
 * Spring MVC controller for 'OtCompany' management.
 */
@RestController
public class OtCompanyRestController extends AbstractRestController {

	@Resource
	private OtCompanyService otCompanyService;
		
	@RequestMapping( value="/otCompany",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<OtCompany> findAll() {
		return otCompanyService.findAll();
	}
	
	@RequestMapping( value="/otCompany/findByCompanyName",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ResponseDto findByCompanyName(@RequestParam(value = "companyName", required = true) String companyName, HttpServletRequest httpServletRequest) {
		
		logger.info("findByCompanyName start");
		
		ResponseDto responseDto = new ResponseDto();
		responseDto.setStatus(ResponseConst.SUCCESS);
		Locale locale = RequestContextUtils.getLocale(httpServletRequest);
		
		try {
			List<OtCompany> otCompanyList = otCompanyService.findByCompanyName(companyName);
			responseDto.setEntity(otCompanyList);
		} catch (Exception e) {
			logger.error("ERROR: ", e);
			responseDto.setStatus(ResponseConst.ERR_SYS);
		}
		
		setMessage(responseDto, locale);
		
		logger.info("findByCompanyName end");
		
		return responseDto;
	}

	@RequestMapping( value="/otCompany/{companyId}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtCompany findById(@PathVariable("companyId") Integer companyId) {
		return otCompanyService.findById(companyId);
	}
	
	@RequestMapping( value="/otCompany",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtCompany create(@RequestBody OtCompany otCompany) {
		return otCompanyService.create(otCompany);
	}

	@RequestMapping( value="/otCompany/{companyId}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public OtCompany update(@PathVariable("companyId") Integer companyId, @RequestBody OtCompany otCompany) {
		return otCompanyService.update(otCompany);
	}

	@RequestMapping( value="/otCompany/{companyId}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void delete(@PathVariable("companyId") Integer companyId) {
		otCompanyService.delete(companyId);
	}
	
}

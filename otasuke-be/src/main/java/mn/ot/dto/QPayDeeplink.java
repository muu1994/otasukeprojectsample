package mn.ot.dto;
/**
 * QPay deep link
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2019.08.11
 */
public class QPayDeeplink {
	private String name;
	private String link;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
}

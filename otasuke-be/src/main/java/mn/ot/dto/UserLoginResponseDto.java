package mn.ot.dto;

import lombok.Data;
import mn.ot.bean.OtUser;

/**
 * User login response DTO
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2019.07.19
 */
public @Data class UserLoginResponseDto {
	private String token;
	private OtUser otUser;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public OtUser getOtUser() {
		return otUser;
	}
	public void setOtUser(OtUser otUser) {
		this.otUser = otUser;
	}
	
}

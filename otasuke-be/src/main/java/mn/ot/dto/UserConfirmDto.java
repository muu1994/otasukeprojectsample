package mn.ot.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
/**
 * User confirmation DTO
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2019.07.19
 */
public class UserConfirmDto {
	
	@NotNull
	private Integer userId;
	@NotEmpty
	@NotNull
	private String confirmCode;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getConfirmCode() {
		return confirmCode;
	}
	public void setConfirmCode(String confirmCode) {
		this.confirmCode = confirmCode;
	}
}

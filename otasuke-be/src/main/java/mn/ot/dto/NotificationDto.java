package mn.ot.dto;

/**
 * Notification DTO
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2021.03.29
 */
public class NotificationDto {
	
	private Integer notificationId;
    private Integer userId;
    private String title;
    private String notifType;
    private String message;
    private String deliveryFlag;
    private String createdDay;
    private String createdTime;
    private String readFlag;
    private String readDate;
    
	public NotificationDto(Integer notificationId, Integer userId,
			String title, String notifType, String message,
			String deliveryFlag, String createdDay, String createdTime,
			String readFlag, String readDate) {
		super();
		this.notificationId = notificationId;
		this.userId = userId;
		this.title = title;
		this.notifType = notifType;
		this.message = message;
		this.deliveryFlag = deliveryFlag;
		this.createdDay = createdDay;
		this.createdTime = createdTime;
		this.readFlag = readFlag;
		this.readDate = readDate;
	}
	
	public Integer getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(Integer notificationId) {
		this.notificationId = notificationId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNotifType() {
		return notifType;
	}
	public void setNotifType(String notifType) {
		this.notifType = notifType;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDeliveryFlag() {
		return deliveryFlag;
	}
	public void setDeliveryFlag(String deliveryFlag) {
		this.deliveryFlag = deliveryFlag;
	}
	public String getCreatedDay() {
		return createdDay;
	}
	public void setCreatedDay(String createdDay) {
		this.createdDay = createdDay;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getReadFlag() {
		return readFlag;
	}
	public void setReadFlag(String readFlag) {
		this.readFlag = readFlag;
	}
	public String getReadDate() {
		return readDate;
	}
	public void setReadDate(String readDate) {
		this.readDate = readDate;
	}
}

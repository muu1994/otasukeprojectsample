package mn.ot.dto;

/**
 * Response DTO
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2019.07.04
 */
public class ResponseDto {
	
	private String status;
	private String message;
	private Object entity;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getEntity() {
		return entity;
	}
	public void setEntity(Object entity) {
		this.entity = entity;
	}	
}

package mn.ot.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Change password DTO
 * @author KhashErdene GANZORIG
 * @date 2019.11.06
 */
public class ChangePhoneDto {
	@NotNull
	private Integer userId;
	@NotNull
	@NotEmpty
	private String password;
	@NotNull
	@NotEmpty
	private String newPhone;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPhone() {
		return newPhone;
	}
	public void setNewPhone(String newPhone) {
		this.newPhone = newPhone;
	}
}

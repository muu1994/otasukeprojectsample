package mn.ot.dto;

import java.util.List;
/**
 * Notification response format DTO
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2019.08.09
 */
public class NotifFormatDto {
	private String createdDay;
	private List<NotificationDto> notificationList;
	public String getCreatedDay() {
		return createdDay;
	}
	public void setCreatedDay(String createdDay) {
		this.createdDay = createdDay;
	}
	public List<NotificationDto> getNotificationList() {
		return notificationList;
	}
	public void setNotificationList(List<NotificationDto> notificationList) {
		this.notificationList = notificationList;
	}
	
}

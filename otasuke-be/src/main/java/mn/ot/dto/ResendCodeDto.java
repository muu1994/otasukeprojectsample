package mn.ot.dto;
/**
 * Resend code DTO
 * @author KhashErdene GANZORIG
 * @date 2019.09.12
 */
public class ResendCodeDto {
	private Integer userId;
	private Integer createdUserId;
	private Integer incidentId;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getCreatedUserId() {
		return createdUserId;
	}
	public void setCreatedUserId(Integer createdUserId) {
		this.createdUserId = createdUserId;
	}
	public Integer getIncidentId() {
		return incidentId;
	}
	public void setIncidentId(Integer incidentId) {
		this.incidentId = incidentId;
	}
}

package mn.ot.dto;
/**
 * User password change DTO
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2019.08.26
 */
public class UserPasswordChangeDto {
	
	private Integer userId;
	private String oldPassword;
	private String newPassword;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}

package mn.ot.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Data;

/**
 * User login request DTO
 * @author GANZORIG KhashErdenerig.Kh
 * @date 2021.03.29
 */
public @Data class UserLoginRequestDto {
	@NotEmpty
	private String username;
	@NotEmpty
	private String password;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}

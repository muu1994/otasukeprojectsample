package mn.ot.dto;
/**
 * User password recovery DTO
 * @author KhashErdene GANZORIG
 * @date 2019.08.26
 */
public class UserPasswordRecoveryDto {
	private String phone;
	private String confirmCode;
	private String newPassword;
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getConfirmCode() {
		return confirmCode;
	}
	public void setConfirmCode(String confirmCode) {
		this.confirmCode = confirmCode;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
}

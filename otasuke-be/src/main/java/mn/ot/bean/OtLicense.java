/*
 * Created on 29 Mar 2021 ( Time 20:36:45 )
 * Generated by GANZORIG KhashErdene ( version 2.1.1 )
 */
package mn.ot.bean;

import java.io.Serializable;

import javax.validation.constraints.*;

import java.util.Date;

public class OtLicense implements Serializable {

    private static final long serialVersionUID = 1L;

    //----------------------------------------------------------------------
    // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
    //----------------------------------------------------------------------
    @NotNull
    private Integer licenseId;

    //----------------------------------------------------------------------
    // ENTITY DATA FIELDS 
    //----------------------------------------------------------------------    

    private Integer userOccupationId;


    private Integer imageId;


    private Date createdDate;



    //----------------------------------------------------------------------
    // GETTER & SETTER FOR THE KEY FIELD
    //----------------------------------------------------------------------
    public void setLicenseId( Integer licenseId ) {
        this.licenseId = licenseId ;
    }

    public Integer getLicenseId() {
        return this.licenseId;
    }


    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    public void setUserOccupationId( Integer userOccupationId ) {
        this.userOccupationId = userOccupationId;
    }
    public Integer getUserOccupationId() {
        return this.userOccupationId;
    }

    public void setImageId( Integer imageId ) {
        this.imageId = imageId;
    }
    public Integer getImageId() {
        return this.imageId;
    }

    public void setCreatedDate( Date createdDate ) {
        this.createdDate = createdDate;
    }
    public Date getCreatedDate() {
        return this.createdDate;
    }


    //----------------------------------------------------------------------
    // toString METHOD
    //----------------------------------------------------------------------
 
        public String toString() { 
        StringBuffer sb = new StringBuffer(); 
        sb.append(licenseId);
        sb.append("|");
        sb.append(userOccupationId);
        sb.append("|");
        sb.append(imageId);
        sb.append("|");
        sb.append(createdDate);
        return sb.toString(); 
    } 


}

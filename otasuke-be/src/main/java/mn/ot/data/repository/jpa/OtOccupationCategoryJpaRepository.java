package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtOccupationCategoryEntity;

/**
 * Repository : OtOccupationCategory.
 */
public interface OtOccupationCategoryJpaRepository extends PagingAndSortingRepository<OtOccupationCategoryEntity, Integer> {

}

package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtPublicHolidayEntity;

/**
 * Repository : OtPublicHoliday.
 */
public interface OtPublicHolidayJpaRepository extends PagingAndSortingRepository<OtPublicHolidayEntity, Integer> {

}

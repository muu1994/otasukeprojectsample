package mn.ot.data.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import mn.ot.bean.jpa.OtCompanyEntity;

/**
 * Repository : OtCompany.
 */
public interface OtCompanyJpaRepository extends PagingAndSortingRepository<OtCompanyEntity, Integer> {
	
	@Query("SELECT e FROM OtCompanyEntity e WHERE lower(e.companyName) like concat('%', :companyName, '%') ")
	List<OtCompanyEntity> findByCompanyName(@Param("companyName") String companyName);
}

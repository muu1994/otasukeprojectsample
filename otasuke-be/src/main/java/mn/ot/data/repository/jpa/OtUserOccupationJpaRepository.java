package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtUserOccupationEntity;

/**
 * Repository : OtUserOccupation.
 */
public interface OtUserOccupationJpaRepository extends PagingAndSortingRepository<OtUserOccupationEntity, Integer> {

}

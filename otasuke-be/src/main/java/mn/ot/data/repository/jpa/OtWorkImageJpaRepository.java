package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtWorkImageEntity;

/**
 * Repository : OtWorkImage.
 */
public interface OtWorkImageJpaRepository extends PagingAndSortingRepository<OtWorkImageEntity, Integer> {

}

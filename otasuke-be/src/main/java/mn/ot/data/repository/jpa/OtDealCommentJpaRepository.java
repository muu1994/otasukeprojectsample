package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtDealCommentEntity;

/**
 * Repository : OtDealComment.
 */
public interface OtDealCommentJpaRepository extends PagingAndSortingRepository<OtDealCommentEntity, Integer> {

}

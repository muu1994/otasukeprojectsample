package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtJpCalendarEntity;

/**
 * Repository : OtJpCalendar.
 */
public interface OtJpCalendarJpaRepository extends PagingAndSortingRepository<OtJpCalendarEntity, Integer> {

}

package mn.ot.data.repository.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtUserEntity;

/**
 * Repository : OtUser.
 */
public interface OtUserJpaRepository extends PagingAndSortingRepository<OtUserEntity, Integer> {
	List<OtUserEntity> findByUsername(String username);
}

package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtNotificationEntity;

/**
 * Repository : OtNotification.
 */
public interface OtNotificationJpaRepository extends PagingAndSortingRepository<OtNotificationEntity, Integer> {

}

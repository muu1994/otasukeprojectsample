package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtTermsOfConditionEntity;

/**
 * Repository : OtTermsOfCondition.
 */
public interface OtTermsOfConditionJpaRepository extends PagingAndSortingRepository<OtTermsOfConditionEntity, Integer> {

}

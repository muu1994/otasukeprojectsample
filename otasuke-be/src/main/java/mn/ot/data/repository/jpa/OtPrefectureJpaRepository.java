package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtPrefectureEntity;

/**
 * Repository : OtPrefecture.
 */
public interface OtPrefectureJpaRepository extends PagingAndSortingRepository<OtPrefectureEntity, Integer> {

}

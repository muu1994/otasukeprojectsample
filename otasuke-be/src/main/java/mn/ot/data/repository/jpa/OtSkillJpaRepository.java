package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtSkillEntity;

/**
 * Repository : OtSkill.
 */
public interface OtSkillJpaRepository extends PagingAndSortingRepository<OtSkillEntity, Integer> {

}

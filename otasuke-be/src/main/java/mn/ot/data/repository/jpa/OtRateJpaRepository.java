package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtRateEntity;

/**
 * Repository : OtRate.
 */
public interface OtRateJpaRepository extends PagingAndSortingRepository<OtRateEntity, Integer> {

}

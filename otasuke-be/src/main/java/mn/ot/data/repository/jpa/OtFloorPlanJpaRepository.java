package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtFloorPlanEntity;

/**
 * Repository : OtFloorPlan.
 */
public interface OtFloorPlanJpaRepository extends PagingAndSortingRepository<OtFloorPlanEntity, Integer> {

}

package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtBuildingTypeEntity;

/**
 * Repository : OtBuildingType.
 */
public interface OtBuildingTypeJpaRepository extends PagingAndSortingRepository<OtBuildingTypeEntity, Integer> {

}

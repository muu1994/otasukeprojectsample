package mn.ot.data.repository.jpa;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import mn.ot.bean.jpa.OtWorkEntity;

/**
 * Repository : OtWork.
 */
public interface OtWorkJpaRepository extends PagingAndSortingRepository<OtWorkEntity, Integer> {

//	@Query("SELECT e FROM OtWorkEntity e WHERE e.workStatus = :workStatus")
//	List<OtWorkEntity> findByWorkStatus(String workStatus, Pageable pageable);
	
	@Query("SELECT e.workId, CASE WHEN (e.workId IN (SELECT f.otWork.workId FROM OtFavouriteEntity f WHERE f.otUser.userId = :userId)) THEN true ELSE false END, "
			+ "e.workStatus, (SELECT MIN(i.imageId) FROM OtWorkImageEntity i WHERE i.otWork.workId = e.workId), "
			+ "(SELECT p.prefectureName FROM OtPrefectureEntity p WHERE e.otPrefecture.prefectureId = p.prefectureId) "
			+ "FROM OtWorkEntity e WHERE e.workStatus = :workStatus")
	List<Object[]> findByWorkStatus(int userId, String workStatus, Pageable pageable);

	@Query("SELECT e.otWork.workId, true, "
			+ "e.otWork.workStatus, (SELECT MIN(i.imageId) FROM OtWorkImageEntity i WHERE i.otWork.workId = e.otWork.workId), "
			+ "(SELECT p.prefectureName FROM OtPrefectureEntity p WHERE e.otWork.otPrefecture.prefectureId = p.prefectureId) "
			+ "FROM OtFavouriteEntity e WHERE e.otUser.userId = :userId")
	List<Object[]> findFavourites(int userId, Pageable pageable);

	@Query("SELECT e.workId, CASE WHEN (e.workId IN (SELECT f.otWork.workId FROM OtFavouriteEntity f WHERE f.otUser.userId = :userId)) THEN true ELSE false END, "
			+ "e.workStatus, (SELECT MIN(i.imageId) FROM OtWorkImageEntity i WHERE i.otWork.workId = e.workId), "
			+ "(SELECT p.prefectureName FROM OtPrefectureEntity p WHERE e.otPrefecture.prefectureId = p.prefectureId) "
			+ "FROM OtWorkEntity e WHERE e.urgencyType = :urgencyType")
	List<Object[]> findByUrgencyType(int userId, String urgencyType, Pageable pageable);

	@Query("SELECT e.workId, CASE WHEN (e.workId IN (SELECT f.otWork.workId FROM OtFavouriteEntity f WHERE f.otUser.userId = :userId)) THEN true ELSE false END, "
			+ "e.workStatus, (SELECT MIN(i.imageId) FROM OtWorkImageEntity i WHERE i.otWork.workId = e.workId), "
			+ "e.otPrefecture.prefectureName "
			+ "FROM OtWorkEntity e WHERE e.otPrefecture.prefectureId = (SELECT u.otPrefecture.prefectureId FROM OtUserEntity u WHERE u.userId = :userId)")
	List<Object[]> findByPrefecture(int userId, Pageable pageable);

	@Query("SELECT e.workId, CASE WHEN (e.workId IN (SELECT f.otWork.workId FROM OtFavouriteEntity f WHERE f.otUser.userId = :userId)) THEN true ELSE false END, "
			+ "e.workStatus, (SELECT MIN(i.imageId) FROM OtWorkImageEntity i WHERE i.otWork.workId = e.workId), "
			+ "(SELECT p.prefectureName FROM OtPrefectureEntity p WHERE e.otPrefecture.prefectureId = p.prefectureId) "
			+ "FROM OtWorkEntity e WHERE e.otWorkCategory.categoryId = :categoryId")
	List<Object[]> findByCategory(int userId, int categoryId, Pageable pageable);
	
}

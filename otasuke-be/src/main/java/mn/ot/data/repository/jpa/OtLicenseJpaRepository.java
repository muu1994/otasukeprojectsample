package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtLicenseEntity;

/**
 * Repository : OtLicense.
 */
public interface OtLicenseJpaRepository extends PagingAndSortingRepository<OtLicenseEntity, Integer> {

}

package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtWorkDealEntity;

/**
 * Repository : OtWorkDeal.
 */
public interface OtWorkDealJpaRepository extends PagingAndSortingRepository<OtWorkDealEntity, Integer> {

}

package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtFavouriteEntity;

/**
 * Repository : OtFavourite.
 */
public interface OtFavouriteJpaRepository extends PagingAndSortingRepository<OtFavouriteEntity, Integer> {

}

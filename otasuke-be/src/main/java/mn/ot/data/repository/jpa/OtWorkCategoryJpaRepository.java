package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtWorkCategoryEntity;

/**
 * Repository : OtWorkCategory.
 */
public interface OtWorkCategoryJpaRepository extends PagingAndSortingRepository<OtWorkCategoryEntity, Integer> {

}

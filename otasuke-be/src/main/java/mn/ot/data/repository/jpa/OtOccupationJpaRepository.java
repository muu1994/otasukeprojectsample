package mn.ot.data.repository.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import mn.ot.bean.jpa.OtOccupationEntity;

/**
 * Repository : OtOccupation.
 */
public interface OtOccupationJpaRepository extends PagingAndSortingRepository<OtOccupationEntity, Integer> {

}
